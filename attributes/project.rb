project = 'tao'

default[project]['type']['type'] = 'php'
default[project]['type']['php']['app_server'] = 'apache'

default[project]['git']['enabled'] = true
default[project]['git']['url'] = 'https://github.com/oat-sa/tao-community.git'
default[project]['git']['revision'] = 'alpha'
# only for closed repos
default[project]['git']['host'] = nil
default[project]['git']['ssh_port'] = nil
default[project]['git']['repo'] = nil

default[project]['domain']            = 'tao.local' # node['fqdn']
default[project]['domain_www_prefix'] = false

default[project]['ipfs']['enabled'] = false
default[project]['mail']['enabled'] = false
default[project]['config']['enabled'] = true

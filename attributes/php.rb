project = 'tao'

default[project]['php']['version_tag']  = '7.2'
default[project]['php']['fpm'] = {}
default[project]['php']['composer']['install'] = true
# https://hub.taocloud.org/articles/administrator-guide/requirements
default[project]['php']['packages'] = [
  'php7.2-mysql',
  'php7.2-curl',
  'php7.2-json',
  'php7.2-gd',
  'php7.2-zip',
  'php7.2-tidy',
  'php7.2-xml',
  'php7.2-mbstring'
]
default[project]['php']['mysql']['install_mysql.so_extension'] = true

include_attribute "tao::database"

default['tao']['tao']['install_method'] = 'config'

# Define other config file
default['tao']['tao']['config_source'] = nil

default['tao']['tao']['admin']['username'] = 'admin'
default['tao']['tao']['admin']['password'] = 'admin'

default['tao']['tao']['namespace'] = "http://#{node['tao']['domain']}/tao.rdf"


# https://hub.taocloud.org/articles/installation-and-upgrading/install-the-tao-platform-from-a-configuration-file
# https://github.com/oat-sa/tao-core/blob/master/scripts/sample/config.json

default['tao']['tao']['config']['super-user'] = {
    "login": node['tao']['tao']['admin']['username'],
    "lastname": "",
    "firstname": "",
    "email": "",
    "password": node['tao']['tao']['admin']['password']
}

default['tao']['tao']['config']['configuration']['global'] = {
    "anonymous_lang": "en-US",
    "lang": "en-US",
    "mode": "debug",
    "instance_name": "tao",
    "namespace": "http://#{node['tao']['domain']}/tao.rdf",
    "url": %Q[http://#{node['tao']['domain']}:8080/],
    "file_path": "/home/tao/www/data/",
    "root_path": "/home/tao/www/",
    "session_name": "tao_123AbC",
    "timezone": "Europe/Luxembourg",
    "import_data": false
}

default['tao']['tao']['config']['configuration']['generis'] = {
  "persistences": {
    "default": {
      "driver": "pdo_mysql",
      "host": "127.0.0.1:3331",
      "dbname":   node['database']['tao']['name'],
      "user":     node['database']['tao']['admin_user'],
      "password": node['database']['tao']['admin_password']
    },
    "cache": {
      "driver": "phpfile"
    },
    "keyValueResult": {
      "driver": "phpredis",
      "host": "127.0.0.1",
      "port": 6379
    }
  },
  "log": [
    {
      "class": "UDPAppender",
      "host": "127.0.0.1",
      "port": 5775,
      "threshold": 1
    },
    {
      "class": "SingleFileAppender",
      "file": "/home/tao/logs/error.log",
      "max_file_size": 1048576,
      "rotation-ratio": 5,
      "format": "%m",
      "threshold": 4
    }
  ]
}


default['tao']['tao']['config']['extensions'] = [
  "taoCe"
]

default['tao']['tao']['config']['postInstall'] = [
  # "testSessionFilesystem": {
  #   "class": "oat\\taoQtiTest\\scripts\\install\\CreateTestSessionFilesystem",
  #   "params": {
  #     }
  #   }
  # }
]

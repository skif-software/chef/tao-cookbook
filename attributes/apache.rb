project = 'tao'

include_attribute "#{project}::php"

default['apache']['mod_php']['module_name'] = 'php7'
default['apache']['mod_php']['package_name'] = "libapache2-mod-php#{node[project]['php']['version_tag']}"
default['apache']['mod_php']['so_filename'] = "libphp#{node[project]['php']['version_tag']}.so"

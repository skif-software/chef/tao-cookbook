# TAO Cookbook Changelog

This file is used to list changes made in each version of the tao cookbook.

# 0.3.0 (2018-10-05)

- Add CHANGELOG
- Add COPYRIGHT
- Add LICENSE
- Add README
- Change domain to localhost in Kitchen environment
- Add vagrant forwarded port 8080

# 0.2.2 (2018-10-04)

- Disable Netdata in kitchen environment
- Enable direct access to apache in kitchen environment
- Fix project git url/revision and missing directories/files

# 0.2.1 (2018-10-04)

- Fix dependency on apache2 cookbook

# 0.2.0 (2018-10-04)

- Implement different installation methods
- Change to Apache2 application server
- Add configuration procedures
- Enable composer install
- Add dependency cookbooks

# 0.1.1 (2018-08-15)

- Enable project cookbook

# 0.1.0 (2018-08-14)

- Create TAO cookbook

#
# Cookbook:: tao-cookbook
# Recipe:: configure
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

package 'unzip'

home = node['project']['project_home']
server_root = File.join(home, 'tao', node['project']['project_server_root'])

# Install the project dependencies
composer_project server_root do
  dev false
  quiet true
  prefer_dist false
  user 'tao'
  group 'tao'
  action :install
end

# Create directories
%w(config data).each do |dir|
  directory File.join(server_root, dir) do
    owner 'tao'
    group 'tao'
    mode 0740 # do not lower permission
    action :create
  end
end

# Place files
%w(index.php .htaccess favicon.ico).each do |file|
  cookbook_file File.join(server_root, file) do
    source file
    owner 'tao'
    group 'tao'
    mode 0640
    action :create
  end
end

# Support different installation methods
case node['tao']['tao']['install_method']
when 'config'
  # Create config file on target system
  file File.join(Chef::Config[:file_cache_path], 'tao.config.json') do
    content node['tao']['tao']['config'].to_json
    owner 'tao'
    group 'tao'
    mode 0640
    action :create
  end unless node['tao']['tao']['config_source'] != nil
  remote_file File.join(Chef::Config[:file_cache_path], 'tao.config.json') do
    source node['tao']['tao']['config_source']
    owner 'tao'
    group 'tao'
    mode 0640
    action :create
  end if node['tao']['tao']['config_source'] != nil
  # Generate installation command
  tao_install_command = "php tao/scripts/taoSetup.php #{File.join(Chef::Config[:file_cache_path], 'tao.config.json')} -v"
when 'cli'
  # Generate installation command
  tao_install_command = %Q[php tao/scripts/taoInstall.php \
    --db_driver pdo_mysql \
    --db_host 127.0.0.1:3331 \
    --db_name #{node['database']['tao']['name']} \
    --db_user #{node['database']['tao']['admin_user']} \
    --db_pass #{node['database']['tao']['admin_password']} \
    --module_namespace #{node['tao']['tao']['namespace']} \
    --module_url http://#{node['tao']['domain']}:8080/ \
    --user_login #{node['tao']['tao']['admin']['username']} \
    --user_pass #{node['tao']['tao']['admin']['password']} \
    -e taoCe
  ]
when 'web'
  # Do not generate installation command
  tao_install_command = nil
end

# Perform installation
execute "install_tao" do
  command tao_install_command
  cwd server_root
  user 'tao'
  group 'tao'
  action :run
end if tao_install_command # otherwise leave that to the user

# Cleanup
file File.join(Chef::Config[:file_cache_path], 'tao.config.json') do
  action :delete
end


# Community extensions installed:
# tao
# taoResultServer
# taoOutcomeRds
# taoDelivery
# taoBackOffice
# taoTestTaker
# taoGroups
# taoItems
# taoTests
# taoQtiItem
# taoQtiTest
# taoDeliveryRdf
# taoOutcomeUi
# taoQtiTestPreviewer
# qtiItemPci
# funcAcl
# taoCe


# hack
# set the server address
# if node["config"]["use-ip"]
#     ruby_block "set server root url" do
#         block do
#             generis_conf = "#{tao_root}/config/generis.conf.php"
#             text = File.read(generis_conf)
#             new_contents = text.gsub(/^define\('ROOT_URL','http:\/\/#{node["config"]["domain"]}\/'\);$/, "define('ROOT_URL', 'http://' . $_SERVER['SERVER_ADDR'] . '/');")
#             puts new_contents
#             File.open(generis_conf, "w") {|file| file.puts new_contents }
#         end
#         action :run
#     end
# end

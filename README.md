# TAO Cookbook

This is a Chef cookbook to bootstrap TAO Community Edition [taotesting.com] on local machine.

## Prerequisites

The following software should be installed on your machine:

* Chef Development Kit [ https://downloads.chef.io/chefdk ]

* VirtualBox [ https://www.virtualbox.org ]

* Vagrant (if MS Windows or macOS) [ https://www.vagrantup.com ]

## Usage

```
git clone https://gitlab.com/skif-software/chef/tao-cookbook.git

cd tao-cookbook

kitchen converge
```
Browse to http://localhost:8080 and enjoy the latest TAO Community Edition.  
Default user: admin  
Default password: admin

## Advanced usage

TODO

name 'tao'
maintainer       'Serge A. Salamanka'
maintainer_email 'salamanka.serge@gmail.com'
license          'Apache v2.0'
description      'Automation procedures for TAO Testing System'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://github.com/skif-software/tao-cookbook'
issues_url 'https://github.com/skif-software/tao-cookbook/issues'
version '0.3.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/tao/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/tao'

depends 'project', '~> 0.7'
